package com.zy.gulimall.authserver.vo;

import lombok.Data;

/**
 * @author zy
 * @version1.0
 */
@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
