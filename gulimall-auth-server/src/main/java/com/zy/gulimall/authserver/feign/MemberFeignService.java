package com.zy.gulimall.authserver.feign;

import com.zy.common.utils.R;
import com.zy.gulimall.authserver.entity.GiteeUserInfo;
import com.zy.gulimall.authserver.vo.UserLoginVo;
import com.zy.gulimall.authserver.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author zy
 * @version1.0
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {
    @PostMapping("/member/member/regist")
     R register(UserRegistVo vo);

    @PostMapping("/member/member/login")
     R login(@RequestBody UserLoginVo vo);

    @PostMapping(value = "/member/member/oauth2/login")
     R oauthLogin(@RequestBody GiteeUserInfo giteeUserInfo);
}
