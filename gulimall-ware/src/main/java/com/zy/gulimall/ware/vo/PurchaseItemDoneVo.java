package com.zy.gulimall.ware.vo;

import lombok.Data;

/**
 * @author zy
 * @version1.0
 */
@Data
public class PurchaseItemDoneVo {
    //{itemId:1,status:4,reason:""}
    private Long itemId;
    private Integer status;
    private String reason;
}
