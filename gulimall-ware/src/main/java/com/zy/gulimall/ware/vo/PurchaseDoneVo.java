package com.zy.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author zy
 * @version1.0
 */
@Data
public class PurchaseDoneVo {

//    @NotNull
    private Long id;//采购单id

    private List<PurchaseItemDoneVo> items;
}

