package com.zy.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy.common.to.OrderTo;
import com.zy.common.to.mq.StockLockedTo;
import com.zy.common.utils.PageUtils;
import com.zy.gulimall.ware.entity.WareSkuEntity;
import com.zy.gulimall.ware.vo.SkuHasStockVo;
import com.zy.gulimall.ware.vo.WareSkuLockVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:36:21
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);


    void unLockStock(StockLockedTo to);

    @Transactional(rollbackFor = Exception.class)
    void unLockStock(OrderTo orderTo);

    void unLockStock(Long skuId, Long wareId, Integer num, Long taskDetailId);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVo vo);


}

