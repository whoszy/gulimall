package com.zy.gulimall.ware.feign;

import com.zy.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author zy
 * @version1.0
 * @Date: 2022/12/04 10:31
 */
@FeignClient("gulimall-order")
public interface OrderFeignService {
    @GetMapping("/order/order/status/{orderSn}")
     R getOrderStatus(@PathVariable("orderSn") String orderSn);
}
