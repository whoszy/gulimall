package com.zy.gulimall.ware.dao;

import com.zy.gulimall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:36:20
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
