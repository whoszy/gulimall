package com.zy.gulimall.ware.vo;

import lombok.Data;

/**
 * @author zy
 * @version1.0
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
