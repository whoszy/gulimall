package com.zy.gulimall.ware.dao;

import com.zy.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:36:21
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
