package com.zy.gulimall.member.vo;

import lombok.Data;

/**
 * @author zy
 * @version1.0
 */
@Data
public class GiteeAccessToken {

    private String access_token;
    private String token_type;
    private long expires_in;
    private String refresh_token;
    private String scope;
    private long created_at;

}