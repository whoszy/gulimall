package com.zy.gulimall.member.dao;

import com.zy.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:22:59
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
