package com.zy.gulimall.member.feign;

import com.zy.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @author zy
 * @version1.0
 * @Date: 2022/12/05 13:37
 */
@FeignClient("gulimall-order")
public interface OrderFeignService {
    @PostMapping("/order/order/listWithItem")
     R listWithItem(@RequestBody Map<String,Object> params);
}
