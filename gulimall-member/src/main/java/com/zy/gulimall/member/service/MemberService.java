package com.zy.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy.common.utils.PageUtils;
import com.zy.gulimall.member.entity.MemberEntity;
import com.zy.gulimall.member.exception.PhoneException;
import com.zy.gulimall.member.exception.UsernameException;
import com.zy.gulimall.member.vo.MemberGiteeUserInfo;
import com.zy.gulimall.member.vo.MemberLoginVo;
import com.zy.gulimall.member.vo.MemberRegistVo;

import java.text.ParseException;
import java.util.Map;

/**
 * 会员
 *
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:22:59
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneException;

    void checkUsernameUnique(String username)throws UsernameException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity login(MemberGiteeUserInfo memberGiteeUserInfo) throws ParseException;
}

