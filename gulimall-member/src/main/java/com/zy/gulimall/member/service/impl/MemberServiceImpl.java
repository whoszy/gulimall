package com.zy.gulimall.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zy.common.utils.PageUtils;
import com.zy.common.utils.Query;
import com.zy.gulimall.member.dao.MemberDao;
import com.zy.gulimall.member.entity.MemberEntity;
import com.zy.gulimall.member.entity.MemberLevelEntity;
import com.zy.gulimall.member.exception.PhoneException;
import com.zy.gulimall.member.exception.UsernameException;
import com.zy.gulimall.member.service.MemberLevelService;
import com.zy.gulimall.member.service.MemberService;
import com.zy.gulimall.member.vo.MemberGiteeUserInfo;
import com.zy.gulimall.member.vo.MemberLoginVo;
import com.zy.gulimall.member.vo.MemberRegistVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Map;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {
    @Autowired
    MemberLevelService memberLevelService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void regist(MemberRegistVo vo) {
        MemberDao baseMapper = this.baseMapper;
        MemberEntity memberEntity = new MemberEntity();

        MemberLevelEntity defaultLevel = memberLevelService.getDefaultLevel();
        memberEntity.setLevelId(defaultLevel.getId());
//检查用户名和手机号是否唯一
        checkPhoneUnique(vo.getPhone());
        checkUsernameUnique(vo.getUserName());
        memberEntity.setRegisterType(0);
        memberEntity.setUsername(vo.getUserName());
        memberEntity.setNickname(vo.getUserName());
        memberEntity.setMobile(vo.getPhone());
        //md5加密
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(vo.getPassword());
        memberEntity.setPassword(encode);

        baseMapper.insert(memberEntity);
    }

    @Override
    public void checkPhoneUnique(String phone) throws PhoneException {
        MemberDao baseMapper = this.baseMapper;
        Integer count = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if (count > 0) {
            throw new PhoneException();
        }

    }

    @Override
    public void checkUsernameUnique(String username) throws UsernameException {
        MemberDao baseMapper = this.baseMapper;
        Integer count = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("username", username));
        if (count > 0) {
            throw new UsernameException();
        }

    }

    @Override
    public MemberEntity login(MemberLoginVo vo) {
        String loginacct = vo.getLoginacct();
        String password = vo.getPassword();
        MemberEntity member = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>()
                .eq("username", loginacct).or().eq("mobile", loginacct).or().eq("email", loginacct));
        String password1 =null;
        if (member != null) {
            password1 = member.getPassword();
        }else {
            return null;
        }

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        boolean matches = bCryptPasswordEncoder.matches(password, password1);
        if (matches) {
            return member;
        }else {
            return null;
        }
    }

    @Override
    public MemberEntity login(MemberGiteeUserInfo memberGiteeUserInfo) throws ParseException {
        //登录和注册合并逻辑
        MemberEntity member = new MemberEntity();
        long socialId = memberGiteeUserInfo.getId();
        MemberDao memberDao = this.baseMapper;
        //判断当前社交用户是否登录过
        MemberEntity socialUid = memberDao.selectOne(new QueryWrapper<MemberEntity>().eq("social_uid", socialId));
        if (socialUid !=null) {
            //这个用户已经注册过了
            MemberEntity entity = new MemberEntity();
            entity.setAccessToken(memberGiteeUserInfo.getAccess_token());
            entity.setId(socialUid.getId());
            entity.setUsername(memberGiteeUserInfo.getName());
            entity.setNickname(memberGiteeUserInfo.getName());
            entity.setRegisterType(1);
            long expiresIn = memberGiteeUserInfo.getExpires_in();
            entity.setExpireIn((int)expiresIn);
            entity.setSocialUid(Long.toString(socialId));
            memberDao.updateById(entity);
            return entity;
        }else {
            //没有就注册
            MemberEntity memberRegist = new MemberEntity();
            try {
                if (socialUid.getId()!=null){
                    memberRegist.setId(socialUid.getId());
                }
                memberRegist.setUsername(memberGiteeUserInfo.getName());
                memberRegist.setNickname(memberGiteeUserInfo.getName());
                memberRegist.setRegisterType(1);
                memberRegist.setLevelId(1L);
            }catch (Exception e){}
            memberRegist.setExpireIn((int) memberGiteeUserInfo.getExpires_in());
            memberRegist.setSocialUid(Long.toString(socialId));
            memberRegist.setAccessToken(memberGiteeUserInfo.getAccess_token());
            memberDao.insert(memberRegist);
            return memberRegist;
        }
    }
}