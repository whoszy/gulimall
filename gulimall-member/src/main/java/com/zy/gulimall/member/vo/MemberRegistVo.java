package com.zy.gulimall.member.vo;

import lombok.Data;

/**
 * @author zy
 * @version1.0
 */
@Data
public class MemberRegistVo {

    private String userName;


    private String password;

    private String phone;
}
