package com.zy.gulimall.product.web;

import com.zy.gulimall.product.entity.CategoryEntity;
import com.zy.gulimall.product.service.CategoryService;
import com.zy.gulimall.product.vo.Catalog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author zy
 * @version1.0
 */
@Controller
public class IndexController {
    @Autowired
    CategoryService categoryService;
    @GetMapping({"/index.html","/"})
    public String indexPage(Model model){
        //查询所有的一级分类
        List<CategoryEntity> entities = categoryService.getLevel1Categoeries();
        model.addAttribute("categories",entities);
        return "index";
    }

    //index/catalog.json
    @GetMapping(value = "/index/catalog.json")
    @ResponseBody
    public Map<String, List<Catalog2Vo>> getCatalogJson() {

        Map<String, List<Catalog2Vo>> catalogJson = categoryService.getCatalogJson();

        return catalogJson;

    }

}
