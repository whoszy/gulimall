package com.zy.gulimall.product.dao;

import com.zy.gulimall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息
 * 
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-10 09:40:41
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {
	
}
