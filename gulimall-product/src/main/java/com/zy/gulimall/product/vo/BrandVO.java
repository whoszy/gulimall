package com.zy.gulimall.product.vo;

import lombok.Data;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: zy
 * @createTime: 2020-05-30 15:39
 **/

@Data
public class BrandVo {

    private Long brandId;

    private String brandName;

}
