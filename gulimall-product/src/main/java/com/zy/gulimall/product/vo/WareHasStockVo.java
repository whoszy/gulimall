package com.zy.gulimall.product.vo;

import lombok.Data;

/**
 * @author zy
 * @version1.0
 */
@Data
public class WareHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
