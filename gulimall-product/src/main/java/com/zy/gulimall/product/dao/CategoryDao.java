package com.zy.gulimall.product.dao;

import com.zy.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-10 09:40:41
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
