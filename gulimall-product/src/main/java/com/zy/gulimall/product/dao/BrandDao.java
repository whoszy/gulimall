package com.zy.gulimall.product.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.gulimall.product.entity.BrandEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author wanwgei
 * @email i@weiwang.com
 * @date 2020-09-13 10:48:45
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {


}
