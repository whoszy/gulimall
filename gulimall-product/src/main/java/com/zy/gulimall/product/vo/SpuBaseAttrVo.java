package com.zy.gulimall.product.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: zy
 * @createTime: 2020-06-19 18:19
 **/

@Data
@ToString
public class SpuBaseAttrVo {

    private String attrName;

    private String attrValue;

}
