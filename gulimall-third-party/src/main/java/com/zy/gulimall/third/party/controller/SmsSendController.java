package com.zy.gulimall.third.party.controller;

import com.zy.common.utils.R;
import com.zy.gulimall.third.party.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author zy
 * @version1.0
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {
    @Autowired
    SmsComponent smsComponent;
    @GetMapping("/sendcode")
    public R sendCode(@RequestParam("phone")String phone,@RequestParam("code")String code) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
    smsComponent.sendSmsCode(phone, code);
    return R.ok();
    }
}
