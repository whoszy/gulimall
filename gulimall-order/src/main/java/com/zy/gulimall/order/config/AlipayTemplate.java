package com.zy.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.zy.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {
    private String timeout = "30m";
    //在支付宝创建的应用的id
    private   String app_id = "2021000121693907";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCHYUEmGop7MLOBQFDKhC6hZSCBtpYdXkCmfiPOw0tGYhn9ueXedNT07/xBrXX8WgOK433hX67YHuO6PUDf/gIYBJIrZLgC3SHshNqzkEWvm9H3MnYh+LKNcQXU5V4nHcJs94Q03IHXuff5snguEjpS2xyNAN3wdHYuNnq+ICasqPOrg/rbOIQpyfDjwhWvvCFDutKfSWa8bK76v5xv+kaFt+y5wbg58O080T6JvgUfSivMrxigySQz9GfJsM23ez3AOP0maACPQ3Orjc5gD6Ikdz/7ZQwNDw7ObuJj+EMvKYc8PMWMKzCsr/flOslEAjwKiGsTVSNIZEmjc47+MEWbAgMBAAECggEAAb1EIwMwSEjl5YE4CIIOP1CXTFNWNmZhmkIAn8VukCbugo4C4ArMOaFjskxSOb3GUeB+W4jpLCmpb1keHkKbMEfOwPP4UUEaEHncfXYbDxiHaA+j6OfTE3IiSwvvW71MJC5B6pPyrxzajMC4Po9JZIsgIMekElTMt+dHSG4NUSaNwj2uIcM1B0VcG/RJf5JeyytO3wZGLD0OSh81LKu8XLRl5wvNwwoZX2dkSGlr5cj7o73CNC6JaguxZ3Q7Tt1TNf99wfApaX812F7vMYkkU8rFw2X7120K97EUDPAKgLgRj4N/PZ6F3PpL11PyN5xhTfHkvS0DCYgZiKvFYEr2YQKBgQC7nvAW7kyQiLH5qa+vaWe62+Yc9VZsXL5gogxCnff6vDhl8fLRdBFH7trGwCvexNlgvOsykl76Dz7ZlIy520McZmNiXUqo16eWOzIZFkvWu9vs/QKUzF3fLTQV3c0FRJfsHHBp2aItgNKA5bmxc8nXk2K8T/q/tlDscOnTHUA3swKBgQC4uDnIH5fL9X+G8Pt1hQBf21WcBgdJ4DgtwqiekeJcredSp6tjLBKZS0xNSCP+diT1aB6EyELDM5MP/LNaEFJBLTs6RmceDcQysmbJNjRUjMHEpuYKlil3KNEScq7Bo0AJ68pN+vfW7wJO+rRPSHAOoL1+ZmSpVYfKarE+mHlGeQKBgA1o0qgQ6A/8dRvu8MuMsQk95jhzoHWmO0UUz+OC/tJA2ZcPr9cggIuvmptR6NDHKPItDnJS287rgAZ1A4nVDfrdpoTFPizLHQMzv+TjHxB95L4PZzvTT3I//NUDQT/7IxCRB8fI8jpmVqU3xgnUyLxLoGbtIHLLSQVdHJkmIgzZAoGBAKTsDuLnmi8G/JptdAWj/waWnOPV2hISd/YbU1NoatNWjS8V9oZv5hcpzHE/lKtvrmXRVVY2Oq1v0cC3XSZvZhE9IiFQlGaX4fFlqTHDDIvktGgwnzkaguEZ9zjiXbcTW2zAMfrXtQoYkVYUroIO6cWVrDo7j2pErB2EjsWVesn5AoGAfRd+BMOiubeCwkvLwog6p1mfNp1Ya5rpoazAuNiEARSXkiljHDNMmeBZoUCTE6B/WL07bFPJt+Vd7VRqAFOh25gMNYKYRvdQ6k3d38RftjJL9NHH3yktu8HijBeeGWKQZM2+6EOBg4QTss/Gw9TkdVsFuqNDZFqUyWyCWGaq0DM=";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAglYSLN4bGPkgmu/xw84mVXLGQOanAlDjsM0rqq9jxOA6Veqqw+OGkRbsXDDGWLHOK6eg0H3SB8k+d6NgQsPo6pMe9zGySkclG0ZVpK4Yfdu14GmOzFaLq+7zDfi7JyvOeroEkFinLMRF1KWNT6Jmnnq8UYazxyUdZWyDjAMdVS180U+ONwtXGGbcoETl3u7Ks/YNmNNCdGwZInMD2RKsQnFazBugWQGU+0l3c5EQp0RFiDr9mjRZOYdhTNHjLY605cKX4Y6zI/V0jvE5a8NtoehUccIeolmAIZTXbeQhN4aC2LUJjRV7SG27KEPwPkx0S8IWfmvSotXBrGw4NjTJXQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url;

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url;

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
