package com.zy.gulimall.order.dao;

import com.zy.gulimall.order.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:32:22
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
