package com.zy.gulimall.order.feign;

import com.zy.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author zy
 * @version1.0
 * @Date: 2022/12/02 12:59
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {
    @GetMapping("/product/spuinfo/skuId/{id}")
     R getSpuInfoBySkuId(@PathVariable("id")Long skuId);
}
