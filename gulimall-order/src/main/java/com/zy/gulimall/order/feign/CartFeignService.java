package com.zy.gulimall.order.feign;

import com.zy.gulimall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author zy
 * @version1.0
 * @Date: 2022/12/01 13:18
 */
@FeignClient("gulimall-cart")
public interface CartFeignService {
    @GetMapping("/currentUserCartItems")
     List<OrderItemVo> getCurrentUserCartItems();
}
