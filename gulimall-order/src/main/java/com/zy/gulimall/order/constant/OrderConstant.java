package com.zy.gulimall.order.constant;

/**
 * @author zy
 * @version1.0
 * @Date: 2022/12/01 22:09
 */
public class OrderConstant {
    public static final String USER_ORDER_TOKEN_PREFIX = "order:token";
}
