package com.zy.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy.common.to.mq.SeckillOrderTo;
import com.zy.common.utils.PageUtils;
import com.zy.gulimall.order.entity.OrderEntity;
import com.zy.gulimall.order.vo.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author zy
 * @email zy991392254@gmail.com
 * @date 2022-11-09 23:32:22
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 订单确认页返回要用的数据
     * @return
     */
    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

    SubmitOrderResponseVo submitOrder(OrderSubmitVo vo);

    OrderEntity getOrderByOrderSn(String orderSn);

    void closeOrder(OrderEntity entity);

    PayVo getOrderPay(String orderSn);

    PageUtils queryPageWithItem(Map<String, Object> params);

    String handlePayResult(PayAsyncVo asyncVo);

    void createSeckillOrder(SeckillOrderTo seckillOrderTo);
}

