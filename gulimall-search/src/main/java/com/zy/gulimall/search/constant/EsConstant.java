package com.zy.gulimall.search.constant;

/**
 * @author zy
 * @version1.0
 */
public class EsConstant {
    public static final String PRODUCT_INDEX = "gulimall_product"; //sku数据在es中的索引
    public static final Integer PRODUCT_PAGESIZE = 16;

}
