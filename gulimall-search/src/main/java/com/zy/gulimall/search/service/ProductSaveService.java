package com.zy.gulimall.search.service;

import com.zy.common.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author zy
 * @version1.0
 */

public interface ProductSaveService  {

    Boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
