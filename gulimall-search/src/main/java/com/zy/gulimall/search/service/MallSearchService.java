package com.zy.gulimall.search.service;

import com.zy.gulimall.search.vo.SearchParam;
import com.zy.gulimall.search.vo.SearchResult;

/**
 * @author zy
 * @version1.0
 */
public interface MallSearchService {
    /**
     *
     * @param param 检索的所有参数
     * @return 检索的结果
     */
    SearchResult search(SearchParam param);


}
